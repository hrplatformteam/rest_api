# --- !Ups
create table employees(username varchar(50) not null unique, firstname varchar(50) not null, lastname varchar(50) not null,
jobtitle varchar(50) not null, location varchar(50) not null, status varchar(50) not null);

insert into employees(username, firstname, lastname, jobtitle, location, status)
values('sk1989', 'shiva', 'komatreddy', 'full stack developer', 'minneapolis', 'full time');

# --- !Downs
drop table if exists employees;
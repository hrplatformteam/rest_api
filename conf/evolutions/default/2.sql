# --- !Ups
create table users(username varchar(50) not null unique, password varchar(50) not null, loggedIn integer not null);

insert into users(username, password, loggedIn) values('sk1989', 'komat123', 0);

# --- !Downs
drop table if exists users;

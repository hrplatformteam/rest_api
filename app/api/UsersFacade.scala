package api

import database.user.{User, UsersDbApi, UsersDbFacade}


object UsersFacade extends UsersApi {

  private val db: UsersDbApi = new UsersDbFacade()

  override def login(username: String, password: String): Option[User] =
    for {
      user <- db find(username, password)
      loggedInUser <- if(user.loggedIn == 0) db login(user.username, user.password) else None
    } yield loggedInUser

  override def logOff(username: String, password: String): Option[User] =
    for {
      user <- db find(username, password)
      loggedOffUser <- db logOff(user.username, user.password)
    } yield loggedOffUser

  override def register(username: String, password: String): Option[User] =
    db add (username, password)

  override def users(): Seq[User] =
    db list

  override def remove(userMessage: UserMessage): Option[String] =
    db delete(userMessage.username, userMessage.password)

}

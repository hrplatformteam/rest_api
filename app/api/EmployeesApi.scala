package api

import database.employee.Employee

trait EmployeesApi {

  def all(): Seq[Employee]

  def byUsername(username: String): Option[Employee]

  def login(msg: UserMessage): Option[Employee]

}
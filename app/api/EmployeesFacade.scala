package api

import database.employee.{Employee, EmployeesDbFacade}


object EmployeesFacade extends EmployeesApi {

  val db = EmployeesDbFacade

  override def all(): Seq[Employee] =
    db.list()

  def byUsername(username: String): Option[Employee] =
    db byUsername username

  override def login(msg: UserMessage): Option[Employee] =
    UsersFacade.login(msg.username, msg.password) match  {
      case Some(user) => db byUsername user.username
      case None => None
    }

}

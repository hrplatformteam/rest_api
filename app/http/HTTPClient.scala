package http

import skinny.http.{HTTP, Request, Response}

object HTTPClient {
  def GET(request: String): Response =
    HTTP.get(request)

  def POST(request: Request): Response =
    HTTP.post(request)

}
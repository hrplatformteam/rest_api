package controllers.util

import api.UserMessage
import database.employee.Employee
import database.user.User
import play.api.libs.json.Json


object JsonFormats {

  implicit val employeeFormat = Json.format[Employee]
  implicit val userFormat = Json.format[User]
  implicit val userMessageFormat = Json.format[UserMessage]

}
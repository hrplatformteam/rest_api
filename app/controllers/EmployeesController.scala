package controllers

import javax.inject._
import api.EmployeesFacade
import util.ResponseTypes._
import util.JsonFormats._
import com.google.inject.Inject
import play.api.libs.json.Json
import play.api.mvc._
import scala.concurrent.Future

@Singleton
class EmployeesController @Inject() (cc: ControllerComponents) extends AbstractController(cc) {

  private val api = EmployeesFacade
  private def badRequest: Future[Result] =
    Future.successful(errorResponse(BAD_REQUEST, Seq("Unable to recognize request")))

  def employees() =  Action.async {
    Future.successful(successResponse(OK, Json.toJson(api.all()), Seq("Successfully processed")))
  }

  def byUsername(username: String) = Action.async {
    api.byUsername(username) match {
      case Some(employee) =>
        Future.successful(successResponse(OK, Json.toJson(employee), Seq(s"Successfully found employee ${employee.username}")))
      case None =>
        Future.successful(errorResponse(NOT_FOUND, Seq("Employee not found")))
    }
  }
}

package database.employee

import anorm._
import database.DatabaseConfig


object EmployeesDbFacade extends EmployeesDbApi {

  private val db = DatabaseConfig.H2Mem
  private val parser = Macro.namedParser[Employee]

  override def list(): Seq[Employee] =
    db.withConnection { implicit connection =>
      SQL("select * from employees").as(parser.*)
    }

  override def byUsername(username: String): Option[Employee] =
    db.withConnection { implicit connection =>
      SQL("select * from employees where username = {username}")
        .on("username" -> username)
        .as(parser.singleOpt)
    }
}
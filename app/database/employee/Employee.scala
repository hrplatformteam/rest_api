package database.employee

case class Employee(username: String,
                    firstname: String,
                    lastname: String,
                    location: String,
                    jobTitle: String,
                    status: String)

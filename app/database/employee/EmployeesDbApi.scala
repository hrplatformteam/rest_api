package database.employee

trait EmployeesDbApi {
  def list(): Seq[Employee]

  def byUsername(username: String): Option[Employee]
}





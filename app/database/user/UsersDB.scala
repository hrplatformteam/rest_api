package database.user

object TempData {
  val shiva = "skomat" ->  User("sk1989", "komat123")
  val carl = "cpage" -> User("cpage", "page123")

  val store = Map(shiva, carl)
}

class UsersDB(users: Map[String, User] = TempData.store, val recent: Option[User] = None) {

  def find(username: String, password: String): Option[User] =
    users.get(username).filter(_.password == password)

  def add(username: String, password: String): Option[UsersDB] =
    find(username, password) match {
      case None =>
        val newUser = User(username, password)
        Some(new UsersDB(users ++ Map(username -> newUser), Some(newUser)))
      case Some(user) => None
    }

  def list(): Seq[User] =
    users.values.toSeq

}

object UsersDB {

  def apply(): UsersDB = new UsersDB

}

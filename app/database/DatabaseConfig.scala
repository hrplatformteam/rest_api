package database

import play.api.db.Databases

object DatabaseConfig {

  val H2Mem = Databases(driver = "org.h2.Driver", url = "jdbc:h2:mem:play")

  val postgres = Databases(driver = "org.postgresql.Driver", url = "jdbc:postgresql://localhost:5432/employees")

}
